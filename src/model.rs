use serde::Deserialize;

#[derive(Deserialize)]
pub enum Gender {
    Male,
    Female,
    Robot,
    BackendDev,
    PreferNotToDiscuss,
}

#[derive(Deserialize)]
pub struct SaveRequest {
    pub name: String,
    pub age: u8,
    pub gender: Gender,
}

#[derive(Deserialize)]
pub struct PINRequest {
    pub pin: String,
}

#[derive(Deserialize, Debug)]
pub struct CoreStatusResponse {
    pub status: String,
}

impl std::fmt::Display for CoreStatusResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        write!(f, "{}", self.status)
    }
}
