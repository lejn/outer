extern crate reqwest;

use actix_web::{web, App, HttpResponse, HttpServer};
use listenfd::ListenFd;
use serde_json::{json, Result};
use uuid::Uuid;
mod model;

async fn health() -> Result<HttpResponse> {
    Ok(HttpResponse::Ok().json(json!({"health": "ok"})))
}

async fn create() -> Result<HttpResponse> {
    let uuid = Uuid::new_v4();
    let pid = &uuid.to_string()[0..8];
    println!("Creating pedestrian with ID {}", pid);

    match snr_send(pid.to_string()).await {
        Ok(v) => Ok(HttpResponse::Created().json(json!({
            "status": "OK".to_string(),
            "message": format!("Pedestrian was created correctly"),
            "pid": pid.to_string(),
            "snr_response": v.to_string(),
        }))),
        Err(e) => Ok(HttpResponse::Created().json(json!({
            "status": "Error".to_string(),
            "message": format!("Error creating a pedestrian: {}", e.to_string()),
        }))),
    }
}

async fn snr_send(pid: String) -> reqwest::Result<serde_json::Value> {
    let pedestrian = json!({
        "name":"Rustacean",
        "age":121,
        "gender":"Male"
    });

    let url = format!("http://localhost:4004/pedestrians/{}", pid);
    let client = reqwest::Client::new();
    let res = client
        .patch(&url.to_string())
        .json(&pedestrian)
        .send()
        .await?
        .json()
        .await?;

    println!("got response from core: {:?}", res);

    Ok(res)
}

async fn save(pid: web::Path<String>, _req: web::Json<model::SaveRequest>) -> Result<HttpResponse> {
    // process request here
    // go to SNR, discobolus, etc.

    // if everything OK, return
    Ok(HttpResponse::Ok().json(json!( {
        "status": "OK".to_string(),
        "message": format!("Pedestrian {} was saved correctly", pid),
    })))
}

async fn pin(pid: web::Path<String>, _req: web::Json<model::PINRequest>) -> Result<HttpResponse> {
    Ok(HttpResponse::Ok().json(json!( {
        "status": "OK".to_string(),
        "message": format!("PIN for pedestrian {} was saved correctly", pid),
    })))
}

async fn submit(pid: web::Path<String>) -> Result<HttpResponse> {
    Ok(HttpResponse::Ok().json(json!( {
        "status": "OK".to_string(),
        "message": format!("Pedestrian {} was submitted", pid),
    })))
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let mut listenfd = ListenFd::from_env();
    let mut server = HttpServer::new(|| {
        App::new().service(
            web::scope("/v1")
                .route("/health", web::get().to(health))
                .route("/pedestrians", web::post().to(create))
                .route("/pedestrians/{pid}", web::patch().to(save))
                .route("/pedestrians/{pid}/submit", web::get().to(submit))
                .route("/pedestrians/{pid}/pin", web::patch().to(pin)),
        )
    });

    server = if let Some(l) = listenfd.take_tcp_listener(0).unwrap() {
        server.listen(l)?
    } else {
        server.bind("0.0.0.0:8000")?
    };

    server.run().await
}
