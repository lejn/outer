### Build

    cargo build

### Run

    cargo run

### Watch

    systemfd --no-pid -s http::3000 -- cargo watch -x run
